# UTT-git
---

L'objectif de ce GitLab est simplement de proposer un repo où tous les cours du semestre A20 seront concentrés afin permettre à tous les A2I d'avoir les cours à jour.
Ainsi, les CM et les TD seront disponibles sur ce git, mais ce ne sera pas le cas des TP (les sujets de TP pourront être présents, mais pas le travail des étudiants dessus).