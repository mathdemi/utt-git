# Complément au cours "CoursVHDL.pdf"

> Exemple 1, Diapo 26

```vhdl
entity Ex1 is -- Ex1.vhd
	port (A,B,C,D : in bit;
	      Q,S : out bit);
end entity;

architecture behav of Ex1 is
	begin
		Q <= (A NOR(NOT B)) XOR (NOT B);
		S <= ((C AND D) XOR C) OR ((C AND D) XNOR D);
	end architecture;
```

> Calcul de parité, Diapo 37
```vhdl
library ieee;
use ieee.std_logic_1164.all;

entity Parity is
	port( 
		Din: in std_logic_vector(7 downto 0); -- donnée 8 bits
		Odd: in std_logic; -- calcul partié impaire si Odd=1
		Par: out std_logic -- bit de parité calculé
	)
end parity;

architecture behav of Parity is
	begin
		process(Din, Odd)
			variable michelDeloizy : integer;
			begin
				if Odd='1' then
					michelDeloizy := 1;
				else
					michelDeloizy := 0;
				end if;
		
				for i in Din'length-1 loop
					if Din(i)='1' then
						michelDeloizy := michelDeloizy + 1;
					end if;
				end loop;
				
				if(x MOD 2) = 1 then
					Par <='1';
				else
					Par <='0';
				end if;
		end process;
end architecture
```

> Autre registre à décalage, Diapo 46
```vhdl
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity SREG08 is
	port(
		clk : in std_logic;							-- horloge
		d_in : in std_logic;						-- bit d'entrée
		pre : in std_logic_vector(7 downto 0);		-- valeur à changer qd load='1'
		load : in std_logic;						-- charge pre dans d_out (asynchrone)
		d_out : in std_logic_vector(7 downto 0);	-- valeur courante du registre à décalage
		match : in std_logic;						-- '1' si pre=d_out
	);
end entity

architecture behav of SREG08 is
	begin
		process(clk, load)
			begin
				if load='1' then
					d_out<=pre;
				else if clk'event and clk='1'
					d_out(6 downto 0) <= d_out(7 downto 1)'driving_value;
					d_out(7) <= d_in>
				end if;
		end process;

		match<='1' when pre=d_out'driving_value else 0;
end architecture
```

> Emission UART, Diapo 47
```vhdl
library ieee;
use iee.std_logic_1164.all;

entity uart_tx is
	port(
		d : in bit_vector(7 downto 0);
		raz : in bit;
		go : in bit;
		clk : in bit; -- horloge
		txd : out bit;
		done : out bit;
	);
end entity

architecture behav of uart_tx is
	type t_etat is (idle, start, data, stop);
	begin
		process(clk, raz)
			st : t_etat := idle;
			buf : bit_vector(7 downto 0);
			nbb : integer range 0 to 7;

			begin
				if raz='1' then
					done<='1';
					txd<='1';
					st := idle;
				else if clk'event and clk='1' then
					case st is 
						when idle => 
							if go='1' then
								buf <= d;
								done <= '0';
								txd <= '0';
								st <= start;
							end if;
						when start =>
							txd <= buf(0);
							nbb := 0;
							st := data;
						when data =>
							if nbb=7 then
								txd <= '1';
								st := stop;
							else
								nbb := nbb+1;
								txd <= buf(ndd);
							end if;
						when stop =>
							done <= '1';
							st := idle;
					end case;
				end if;
		end process
end architecture
```