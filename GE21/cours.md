# GE21: Droit de l'entreprise

> Contrat synallagmatique : Contrat par lequel les parties s'obligent réciproquement l'un envers l'autre.

Le Droit offre des droits et des libertés.

## I - Les institutions judiciaires

### 1 - Les règles de Droit (_RdD_)

Lavie en société apporte des règles. Le Droit constitue un ensemble de ces règles. En plus des _RdD_, nous suivons d'autres règles, il faut donc savoir identifier les _RdD_ puisqu'il s'agit des seules règles s'appliquant à tous.

La _RdD_ a pour objet d'organiser la vie en société, et d'organiser les relations entre les membres de cette société. Cette définition est encore incomplète, puisque les autres règles peuvent avoir le même objet, comme par exemple la Politesse, la Religion ou encore la Morale.

La _RdD_ est coercitive, c'est-à-dire qu'elle exerce des contraintes. Ce qui la distingue des autres règles sur ce point, c'est que la sanction est délivrée par l'Etat.

> Comment identifier une _RdD_ ?

Le rôle d'une _RdD_ est donc d'imposer un comportement à une personne, si celle-ci est concernée par la _RdD_. Par exemple :

[Tout fait quelconque de l'homme, qui cause à autrui un dommage, oblige celui par la faute duquel il est arrivé à le réparer.](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000032041571)

Le mariage est prohibé entre ascendants et descendants d'une même ligne, entre frères, entre soeurs, entre frère et soeur, entre un oncle et sa nièce ou son neveu, entre une tante et sa nièce ou son neveu. ([Articles 161 à 163](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070721/LEGISCTA000006117710/#LEGISCTA000006117710))

[Si un individu signe un contrat, il doit s'y tenir.](https://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques/Contrat)

- La _RdD_ a un caractère général et impersonnel.

Elle concerne tout le monde mais ne désigne pas une personne en particulier, sauf dans le cadre d'un jugement.

La _RdD_ est en réalité plus relative que générale, car elle ne s'applique qu'à certaines situations (handicap, mariage, contrat,...), elles peuvent donc ne s'appliquer qu'à certaines catégories de personnes (salariés, propriétaires,...).

> En vertu de la séparation des pouvoirs, le Pouvoir judiciaire (par exemple, un juge) ne peut pas émettre une _RdD_, ce qui appartient au Pouvoir législatif.

La relativité du caractère générale des règles concerne certainement plus la _RdD_ que les autres règles comme la Règle de Morale (_RdM_) ou la Règle de Religion (_RdR_). Par exemple, une _RdR_ s'applique à tous les pratiquants.

- Les finalités sociales des _RdD_, _RdM_ et _RdR_ sont différentes.

Les _RdM_ et _RdR_ sont plus tournées vers l'individu que vers la société. La _RdR_ a pour objectif l'ordre social (ordre et sécurité).

> L'organisation des relations sociales par le Droit amène aussi des envies de Justice, d'Harmonie et d'Equilibre, ce qui est fondamentalement tourné vers le bien-être de l'individu.

Il peut y avoir des conflits entre la _RdD_ et la _RdM_ ou la _RdR_. Par exemple, la prescription extinctive existe en _RdD_ mais pas en _RdM_.

> Le Droit n'a pas à être moral.

Cependant, _RdD_, _RdM_ et _RdR_ peuvent se rapprocher sur certains points, comme l'interdiction de tuer, de voler, de faire de faux témoignages,...

De plus, le Droit n'ignore pas la Religion. Comme mentionné dans l'[Article 1 de la loi du 9 décembre 1905 concernant la séparation de l'Eglise et de l'Etat](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000006340313), le libre exercice des cultes est garantit par le Droit.

De même, la Morale prend en compte les autres personnes, quand bien même elle est centrée sur le soi. Le Droit prend également compte de la Morale sur certaines questions, comme par exemple la GPA, la PMA, l'euthanasie,...